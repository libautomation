#include <limits.h>
#include <string.h>
#include <ev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <glob.h>
#include <stdint.h>

#include "libautomation.h"

int atm_file_integer(const char *filename, int *v) {
	int fd, ret;
	char buf[256];

	fd = open(filename, O_RDONLY);
	if(fd < 0)
		atm_fail(filename);

	ret = read(fd, buf, 256);
	close(fd);
	if(ret < 0)
		atm_log("read_device(%s): %s", filename, strerror(errno));
	else {
		*v = atm_read_int_or_fail(buf, filename);

		return 0;
	}

	return -1;
}

int atm_file_float(const char *filename, float *v) {
	int fd, ret;
	char buf[256];

	fd = open(filename, O_RDONLY);
	if(fd < 0)
		atm_fail(filename);

	ret = read(fd, buf, 256);
	close(fd);
	if(ret < 0)
		atm_log("read_device(%s): %s", filename, strerror(errno));
	else {
		*v = atm_read_float_or_fail(buf, filename);

		return 0;
	}

	return -1;
}

int atm_ds_filter_cb(struct ATM_DS_FILTER *transf, int *v) {
	int value, ret;

	ret = transf->src.cb(transf->src.private_data, &value);
	if (ret == 0) {
		value = transf->func(transf->data, value);
		if (value == INT_MAX)
			return -1;
		*v = value;
	}

	return ret;
}

static int atm_dsgrp_dsgrp_cb (struct ATM_DSGRP *dsgrp) {
	return dsgrp->policy(dsgrp);
}

int atm_ds_policyhelper(struct ATM_DS *ds) {
	int ret = ds->src.cb(ds->src.private_data, &ds->dest->v);

	if (ret == 0)
		ds->dest->ts = atm_time;

	return ret;
}

int atm_ds_policy_ignore(struct ATM_DSGRP *grp) {
	int ret;

	while (grp->cur < grp->n) {
		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		if (ret > 0)
			return ret;
		++grp->cur;
	}

	grp->cur = 0;

	return 0;
}

int atm_ds_policy_detect(struct ATM_DSGRP *grp) {
	int ret;

	while (grp->cur < grp->n) {
		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		++grp->cur;
		if (ret != 0)
			return ret;
	}

	grp->cur = 0;

	return 0;
}

int atm_ds_policy_retry(struct ATM_DSGRP *grp) {
	int max_retries = (intptr_t) grp->policy_data, retries = 0, ret = 0;

	while (grp->cur < grp->n && retries < max_retries) {
		++retries;

		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		if (ret > 0)
			return ret;
		if (ret == 0) {
			++grp->cur;
			retries = 0;
		}
	}

	grp->cur = 0;

	return ret;
}

int atm_ds_policy_reset(struct ATM_DSGRP *grp) {
	struct ATM_DSGRP_PD_RESET *d = grp->policy_data;
	int ret;

	if (d->status == -1) {
		d->status = 0;
		d->enable(d->enable_data);

		return d->delay;
	}

	while (grp->cur < grp->n) {
		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		if (ret > 0)
			return ret;
		if (ret < 0)
			++d->status;
	}
	grp->cur = 0;

	if (d->status == 0)
		return 0;

	d->status = -1;
	d->disable(d->disable_data);

	return d->delay;
}

static struct ATM_DS_BARE *_atm_iiosysfs_setup(const char *dev,
			const char *channel, struct ATM_DS_BARE *src) {
	struct ATM_DS_FILTER *transf;
	struct ATM_FILTER_SCALE_OFFSET *filter;
	glob_t globbuf;
	char buffer[NAME_MAX], *p, *filename;
	int fd, i, size, offset = 0;
	float scale = 1.0;

	if (dev[0] == '/') {
		glob(dev, GLOB_MARK, NULL, &globbuf);
		if (globbuf.gl_pathc == 0)
			goto err_glob;

		size = strlen(*globbuf.gl_pathv);
		i = 0;
	}
	else {
		glob("/sys/bus/iio/iio:device*/name", 0, NULL, &globbuf);
		for (i = 0; i < globbuf.gl_pathc; ++i) {
			fd = open(globbuf.gl_pathv[i], O_RDONLY);
			size = read(fd, buffer, sizeof(buffer));
			close(fd);
			if (!strncmp(buffer, dev, size)) /* TODO: linebreak */
				break;
		}
		if (i == globbuf.gl_pathc)
			goto err_glob;

		size = strlen(globbuf.gl_pathv[i]) - 4;
		globbuf.gl_pathv[i][size] = 0;
	}
	size = sizeof(buffer);
	p = atm_stradd(buffer, globbuf.gl_pathv[i], &size);
	p = atm_stradd(p, channel, &size);
	if (size < 8)
		goto err_glob;

	strcpy(p, "_input");
	glob(buffer, 0, NULL, &globbuf);
	if (globbuf.gl_pathc == 1) {
		filename = strdup(buffer);
	}
	else {
		strcpy(p, "_raw");
		glob(buffer, 0, NULL, &globbuf);
		if (globbuf.gl_pathc == 0)
			goto err_glob;

		filename = strdup(buffer);
		strcpy(p, "_scale");
		atm_file_float(buffer, &scale);
		strcpy(p, "_offset");
		atm_file_integer(buffer, &offset);
	}

	if (offset != 0 || scale != 1.0) {
		filter = malloc(sizeof(*filter));
		filter->scale = scale;
		filter->offset = offset;

		transf = malloc(sizeof(*transf));
		transf->func = atm_filter_scale_offset;
		transf->data = filter;
		atm_ds_set_char(&transf->src, filename, atm_file_integer);

		atm_ds_set_dsfilter(src, transf);
	}
	else {
		atm_ds_set_char(src, filename, atm_file_integer);
	}

	return src;

err_glob:
	globfree(&globbuf);

	return NULL;
}

static int filter_setup_find_next(char *s, char **end) {
	char *start;

	start = strstr(s, ",next=");
	if (start)
		*end = start + sizeof(",next=") - 1;

	return start - s;
}

/* Returns an empty data source, on success the new, else the old one. */
static struct ATM_DS_BARE *_atm_filter_setup(struct ATM_DS_BARE *src, char **url) {
	struct ATM_FILTER_SCALE_OFFSET f;
	struct ATM_DS_FILTER *transf;
	char *p = *url;
	char buffer[NAME_MAX];
	int len, n = 0, init = 0;

	transf = malloc(sizeof(*transf));
	atm_ds_set_dsfilter(src, transf);

	switch (*p) {
	case 'l':
		if (strncmp(p, "logger,file=", 12) == 0) {
			len = filter_setup_find_next(&p[12], url);
			if (len > 0) {
				strncpy(buffer, &p[12], len);
				buffer[len] = 0;
				transf->data = fopen(buffer, "a");
				transf->func = atm_filter_logger;

				if (transf->data)
					return &transf->src;
			}
		}
		break;
	case 'm':
		if (sscanf(p, "moving_average, %d, init=%d%n", &n, &init, &len) == 2) {
			transf->data = atm_filter_moving_average_create(n, init);
			transf->func = atm_filter_moving_average;

			goto success;
		}
		break;
	case 's':
		if (sscanf(p, "scale=%f_offset=%d%n", &f.scale, &f.offset, &len) == 2) {
			transf->data = malloc(sizeof(f));
			*(struct ATM_FILTER_SCALE_OFFSET *) transf->data = f;
			transf->func = atm_filter_scale_offset;

			goto success;
		}
		if (sscanf(p, "slr, %d%n", &n, &len) == 1) {
			int unit = 1;
			transf->data = atm_filter_slr_create(n);
			transf->func = atm_filter_slr;
			p += len;
			len = 0;
			sscanf(p, " , unit=%i%n", &unit, &len);
			((struct ATM_FILTER_SLR *) transf->data)->squnit = unit*unit;

			goto success;
		}
		break;
	case 'v':
		if (sscanf(p, "variance, %d%n", &n, &len) == 1) {
			int unit = 1;
			transf->data = atm_filter_variance_create(n);
			transf->func = atm_filter_variance;
			p += len;
			len = 0;
			sscanf(p, " , unit=%i%n", &unit, &len);
			((struct ATM_FILTER_VARIANCE *) transf->data)->squnit = unit*unit;

			goto success;
		}
	}

	atm_log("Unknown filter: '%s'", p);
	free(transf);

	return src;

success:
	filter_setup_find_next(&p[len], url);
	return &transf->src;
}

static int dummy_cb(void *p, volatile atm_shm_t *d) {
	*d = (intptr_t) p;
	return 0;
}

static struct ATM_DS_BARE *_atm_ds_register(struct ATM_DS_BARE *src, char *url) {
	char *p;
	int i;

	if (!url)
		return NULL;

	switch (*url) {
	case 'd':
		if (sscanf(url, "dummy:%d", &i) == 1) {
			src->cb = dummy_cb;
			src->private_data = (void *) (intptr_t) i;
			return src;
		}
	case 'f':
		if (strncmp(url, "file:", 5) == 0) {
			src->private_data = atm_globdup(&url[5]);
			if (!src->private_data) {
				atm_log("Can't find %s", url);
				return NULL;
			}

			src->cb = (int (*)(void *, volatile atm_shm_t *)) atm_file_integer;
			return src;

		} else if (strncmp(url, "filter:", 7) == 0) {
			p = &url[7];
			src = _atm_filter_setup(src, &p);
			return _atm_ds_register(src, p);
		}
	case 'i':
		if (strncmp(url, "iiosysfs:", 9) == 0) {
			url += 9;
			p = strchr(url, ':');
			if (!p)
				return NULL;
			*p++ = 0;

			return _atm_iiosysfs_setup(url, p, src);
		}
	}

	atm_log("Invalid pseudo-url schema in: '%s'", url);
	return NULL;
}

struct ATM_VALUE *
atm_ds_register(struct ATM_DSGRP *grp, char *url, const char *key) {
	struct ATM_DS *new_entry;

	switch (*url) {
	case 'l':
		if (strncmp(url, "local:", 6) == 0) {
			struct ATM_VALUE *v;
			if (!atm_shm_stdmem) {
				atm_log("no data for %s", url);
				return NULL;
			}

			v = atm_shm_find(atm_shm_stdmem, &url[6]);
			/* Create if we reference ourself: */
			if (!v && !strcmp(key, &url[6]))
				v = atm_value(key);

			return v;
		}

		break;

	case 's':
		if (strncmp(url, "shm:", 4) == 0)
			return atm_shm_get_by_url(&url[4]);

		break;

	case 'v':
		if (strncmp(url, "value:", 6) == 0) {
			struct ATM_VALUE *v = atm_value(key);

			atm_shm_update(v, strtol(&url[6], NULL, 10));

			return v;
		}

		break;
	}

	/* We actually need to setup a value in shared memory */
	new_entry = atm_dsgrp_next(grp);
	if (_atm_ds_register(&new_entry->src, url)) {
		grp->n++;
		new_entry->dest = atm_value(key);
		return new_entry->dest;
	}

	return NULL;
}

struct ATM_DS *atm_dsgrp_next(struct ATM_DSGRP *grp) {
	if (grp->n >= grp->size) {
		grp->size <<= 1;
		grp->memb = realloc(grp->memb, grp->size * sizeof(*grp->memb));
	}

	return &grp->memb[grp->n];
}


struct ATM_DS *atm_dsgrp_insert(struct ATM_DSGRP *grp, void *cb, void *pdata,
				struct ATM_VALUE *dest) {
	struct ATM_DS *next = atm_dsgrp_next(grp);

	grp->n++;

	next->src.cb = cb;
	next->src.private_data = pdata;
	next->dest = dest;

	return next;
}

struct ATM_VALUE *atm_ds(char *url, const char *key) {
	return atm_ds_register(&atm_main_task.dsgrp, url, key);
}

struct ATM_DS_BARE *atm_setup_filterchain(struct ATM_DSGRP *grp, char *url,
					  const char *key, struct ATM_VALUE **dest) {
	int old_n = grp->n;
	struct ATM_DS_BARE *dsb;
	struct ATM_VALUE *p;

	p = atm_ds_register(grp, url, key);
	if (dest)
		*dest = p;

	if (old_n == grp->n)
		return NULL;

	dsb = &grp->memb[old_n].src;
	while (dsb->cb == (int (*)(void *, volatile atm_shm_t *)) atm_ds_filter_cb) {
		struct ATM_DS_FILTER *f = dsb->private_data;
		dsb = &f->src;
	}

	return dsb;
}

struct ATM_DSGRP *atm_dsgrp_dsgrp(struct ATM_DSGRP *grp) {
	static struct ATM_VALUE dummy_dest;  /* for garbage but writeable ts? */
	struct ATM_DSGRP *newgrp = malloc(sizeof(*newgrp));

	atm_dsgrp_init(newgrp);
	atm_dsgrp_insert(grp, atm_dsgrp_dsgrp_cb, newgrp, &dummy_dest);

	return newgrp;
}

void atm_dsgrp_init(struct ATM_DSGRP *grp) {
	grp->cur = grp->n = 0;
	grp->size = 4;
	grp->memb = malloc(grp->size * sizeof(*grp->memb));
	grp->policy_data = NULL,
	grp->policy = atm_ds_policy_ignore;
}
