#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include "libautomation.h"

int atm_filter_scale_offset(void *filter_data, int v) {
	struct ATM_FILTER_SCALE_OFFSET *f = filter_data;
	float ret = f->scale * v + f->offset;

	return fabs(ret) > INT_MAX ? INT_MAX : (int) ret;
}

int atm_filter_moving_average(void *f_mov_average, int val) {
	struct ATM_FILTER_MOVING_AVERAGE *filter = f_mov_average;

	filter->value -= filter->data[filter->pos];
	filter->data[filter->pos] = val;
	if (++filter->pos >= filter->length)
		filter->pos = 0;

	return (filter->value += val) / filter->length;
}

struct ATM_FILTER_MOVING_AVERAGE *atm_filter_moving_average_create
						(int length, int init_value) {
	struct ATM_FILTER_MOVING_AVERAGE *f =
				malloc(sizeof(*f) + length * sizeof(int));
	int i;

	f->length = length;
	f->pos = 0;
	f->data = (void *) f + sizeof(*f);
	f->value = init_value * length;

	for(i = 0; i < length; ++i)
		f->data[i] = init_value;

	return f;
}

static inline int sq(int v) {
	return __builtin_smul_overflow(v, v, &v) ? -1 : v;
}

struct _ATM_VARIANCE_DATA {
	int value;
	int variance;  /* Negativ value: The data was excluded from average computation. */
};

int atm_filter_variance(void *f_variance, int val) {
	struct ATM_FILTER_VARIANCE *filter = f_variance;
	int variance;

	if (filter->valid == 0) {
		filter->value = val;
		filter->data[filter->pos].value = val;
		filter->data[filter->pos].variance = 0;
		++filter->valid;
	}
	else {
		variance = sq(filter->value/filter->valid - val);

		if (filter->data[filter->pos].variance >= 0) {
			filter->value -= filter->data[filter->pos].value;
			filter->variance -= filter->data[filter->pos].variance;
			filter->valid--;
		}

		filter->data[filter->pos].value = val;

		if (filter->valid > filter->length / 2 &&
				variance > filter->variance / (filter->valid - 1) +
				filter->squnit)
			variance = -1;  /* Ignore this data point */

		if (variance >= 0) {
			filter->variance += variance;
			filter->value += val;
			filter->valid++;
		}
		filter->data[filter->pos].variance = variance;
	}

	if (++filter->pos >= filter->length)
		filter->pos = 0;

	return variance < 0 ? INT_MAX : filter->value/filter->valid;
}

struct ATM_FILTER_VARIANCE *atm_filter_variance_create(int length) {
	struct ATM_FILTER_VARIANCE *f = malloc(sizeof(*f) + length * sizeof(*f->data));
	int i;

	f->length = length;
	f->pos = 0;
	f->data = (void *) f + sizeof(*f);
	f->value = 0;
	f->valid = 0;
	f->variance = 0;
	f->squnit = 1;

	for(i = 0; i < length; ++i)
		f->data[i].variance = -1;

	return f;
}

struct _ATM_SLR_DATA {
	int x;		/* value < -time_offset: Uninitialized or dropped after overflow */	
	int y;
	int variance;  /* Negativ value: The data was excluded from regression computation. */
};

static int slr_time_range_check(struct ATM_FILTER_SLR *filter, int time_range) {
		/* 2*sqrt(MAX_INT)/len as rough estimate for sqrt(MAX_INT/len) */
	return time_range > (2 << (sizeof(int) * 4)) / filter->length;
}

int atm_filter_slr_estimate(struct ATM_FILTER_SLR *filter, int time) {
	int64_t est;

	if (filter->valid == 0)
		return INT_MAX;

	est = time - filter->time_offset;
	est *= filter->b1_num;
	est /= filter->b1_denom;

	return filter->value_offset + filter->b0 + (int) est;
}

static void slr_recompute(struct ATM_FILTER_SLR *filter) {
	struct _ATM_SLR_DATA *d;
	int oldest, time_shift, value_shift, old_variance, valid_time;

	oldest = filter->pos + 1;
	if (oldest >= filter->length)
		oldest = 0;
	
	while (oldest != filter->pos) {
		if (!slr_time_range_check(filter, filter->data[filter->pos].x - filter->data[oldest].x))
			break;

		filter->data[oldest].x = -filter->time_offset - 1;
		filter->data[oldest].variance = -1;

		if (++oldest >= filter->length)
			oldest = 0;
	}

	time_shift = (filter->data[filter->pos].x + filter->data[oldest].x) / 2;
	value_shift = filter->value / filter->valid;

	old_variance = filter->valid > 4 ? filter->variance / filter->valid : UINT_MAX / filter->length;

	filter->variance = 0;
	filter->valid = 1;   /* Trick to make the estimator above happy. Undo below */
	filter->timesum = 0;
	filter->time_variance = 0;
	filter->value = 0;
	filter->correl_sum = 0;

	valid_time = -filter->time_offset - time_shift;

	for (d = filter->data; d < &filter->data[filter->length]; ++d) {
		d->variance = sq(atm_filter_slr_estimate(filter, d->x + filter->time_offset) - d->y - filter->value_offset);
		d->x -= time_shift;
		d->y -= value_shift;
		if (d->x < valid_time)
			continue;

		if (d->variance > 2*old_variance + filter->squnit)
			d->variance = -1;
		if (d->variance < 0)
			continue;

		filter->variance += d->variance;
		++filter->valid;
		filter->timesum += d->x;
		filter->time_variance += d->x * d->x;
		filter->value += d->y;
		filter->correl_sum += d->x * d->y;
	}

	filter->valid--;  /* Undo the above Trick */

	filter->time_offset += time_shift;
	filter->value_offset += value_shift;
}

int atm_filter_slr(void *f_slr, int val) {
	struct ATM_FILTER_SLR *filter = f_slr;
	struct _ATM_SLR_DATA *d = &filter->data[filter->pos];
	int64_t i64;
	int variance, i;

	if (filter->data[filter->pos].variance >= 0) {
		filter->timesum -= d->x;
		filter->time_variance -= sq(d->x);
		filter->value -= d->y;
		filter->variance -= d->variance;
		filter->correl_sum -= d->x * d->y;
		filter->valid--;
	}

	if (filter->valid == 0) {
		filter->value_offset = val;
		filter->time_offset = atm_time;
		d->y = 0;
		d->x = 0;
		d->variance = 0;
		++filter->valid;
	}
	else {
		variance = sq(atm_filter_slr_estimate(filter, atm_time) - val);

		d->x = atm_time - filter->time_offset;
		d->y = val - filter->value_offset;

		if (filter->valid > filter->length / 2 &&
				variance > filter->variance / (filter->valid - 1) +
				filter->squnit)
			variance = -1;  /* Ignore this data point */

		d->variance = variance;

		if (variance >= 0) {
			if (slr_time_range_check(filter, d->x) ||
			    __builtin_uadd_overflow(filter->variance, variance, &filter->variance) ||
			    __builtin_smul_overflow(d->x, d->y, &i) ||
			    __builtin_sadd_overflow(filter->correl_sum, i, &filter->correl_sum)) {
				slr_recompute(filter);
			}
			else {
				filter->value += d->y;
				filter->timesum += d->x;
				filter->time_variance += sq(d->x);
				filter->valid++;
			}
		}
	}

	while (__builtin_smul_overflow(filter->value, filter->timesum, &i)) 
		slr_recompute(filter);

	if (filter->valid == 0)
		return INT_MAX; /* TODO: slr_recompute() should be smarter */

	filter->b1_num = filter->correl_sum - i / filter->valid;
	filter->b1_denom = filter->time_variance - filter->timesum * filter->timesum / filter->valid;
	if (filter->b1_denom == 0)
		filter->b1_denom = 1;

	i64 = filter->timesum;
	i64 *= filter->b1_num;
	i64 /= filter->b1_denom;
	filter->b0 = filter->value - (int) i64;
	filter->b0 /= filter->valid;

	if (++filter->pos >= filter->length)
		filter->pos = 0;

	return variance < 0 ? INT_MAX : atm_filter_slr_estimate(filter, atm_time);
}

struct ATM_FILTER_SLR *atm_filter_slr_create(int length) {
	struct ATM_FILTER_SLR *f = calloc(sizeof(*f) + length * sizeof(*f->data), 1);
	int i;

	f->length = length;
	f->data = (void *) f + sizeof(*f);
	f->b1_denom = 1;
	f->squnit = 1;

	for(i = 0; i < length; ++i) {
		f->data[i].variance = -1;
		f->data[i].x = -1;
	}

	return f;
}

int atm_filter_logger(void *stream, int value) {
	FILE *fp = (FILE *) stream;

	fprintf(fp, "%d\t%d\n", atm_time, value);

	return value;
}
