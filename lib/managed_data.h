#ifndef _MANAGEDDATA_H
#define _MANAGEDDATA_H

#include <search.h>
#include <stdlib.h>
#include <string.h>

/* Collection of primitive data structures that for some reason are still
 * not available as C library. Since this is only a header file, all functions
 * have to be defined as inline.
 */

#ifndef MD_DECL
#define MD_DECL static inline
#endif

struct MD_LUT_ENTRY {
	const char *key;
	void *data;
};

struct MD_LUT {
	size_t n;
	size_t size;
	struct MD_LUT_ENTRY *start;
};

MD_DECL int _md_lut_compare(const void *key, const void *entry) {
	const struct MD_LUT_ENTRY *e = entry;
	return strcmp((const char *) key, e->key);
}

MD_DECL struct MD_LUT_ENTRY *md_lut_find(struct MD_LUT *t, const char *key) {

	return lfind(key, t->start, &t->n, sizeof(struct MD_LUT_ENTRY),
		     _md_lut_compare);
}

MD_DECL void md_lut_insert(struct MD_LUT *t, const char *key, void *data) {
	if (t->n == t->size) {
		t->size <<= 1;
		t->start = realloc(t->start, t->size * sizeof(*t->start));
	}

	t->start[t->n].key = key;
	t->start[t->n].data = data;
	t->n++;
}

MD_DECL void md_lut_init(struct MD_LUT *t) {
	t->size = 8;
	t->n = 0;
	t->start = malloc(8 * sizeof(*t->start));
}

#endif
