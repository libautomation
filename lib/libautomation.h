#ifndef _LIBAUTOMATION_H
#define _LIBAUTOMATION_H

#include <ev.h>

/*
 * Generally useful functions
 */
/* TODO: Should this follow syslog() or do_log() ... */
extern void atm_log(const char *fmt, ...);
extern void atm_fail(const char *message);
extern int atm_echo(const char *str, const char *filename);
/* Current time in 0.1 seconds resolution: */
extern int atm_timestamp(void);
extern long int atm_read_int_or_fail(const char *str, const char *errmsg);
extern float atm_read_float_or_fail(const char *str, const char *msg);
extern char *atm_stradd(char *dest, const char *source, int *size);
extern void atm_call_in(ev_tstamp seconds, void *func_pointer);

/* return NULL if pattern doesn't match any file */
extern char *atm_globdup(const char *pattern);

extern void atm_do_nothing(void);

/* return 0 if value was updated successfully */
extern int atm_file_float(const char *filename, float *value);
extern int atm_file_integer(const char *filename, int *value);


/*
 * Basic infrastructure
 */

/* start the event loop, probably doesn't return */
extern int atm_main(int argc, char **argv);

/* Time when the current/last task was called. 0.1 seconds resolution like atm_timestamp() */
extern int atm_time;
extern struct ev_loop *loop;
extern struct ATM_TASK atm_main_task;
#define atm_main_task_timer ((ev_timer *) &atm_main_task)


/*
 * Basic data types
 */

typedef volatile int atm_shm_t;

struct ATM_VALUE {
	atm_shm_t v;	/* the value */
	atm_shm_t ts;	/* last update */
};

/* Create a new variable in standard shared memory area or on the heap
   if shared memory is not available or key == NULL.*/
extern struct ATM_VALUE *atm_value(const char *key);

/* Time since last update of value in units of 0.1 seconds */
static inline int atm_value_age(struct ATM_VALUE *value) {
	return atm_time - value->ts;
}

struct ATM_SHM_HEADER {
	int ks;
	int value_count;
	unsigned char magic;
};

/* This is supposed to be an opaque data structure. We might change it
   without notice. So typedef it. */
typedef struct {
	struct ATM_SHM_HEADER *header;
	char *keys;
	struct ATM_VALUE *data;
	int fd;
} *ATM_SHM;

/*
 * Shared memory interface - client side
 */

/* Low-level API - mostly for internal use */

extern ATM_SHM atm_shm_attach(const char *id);
extern struct ATM_VALUE *atm_shm_find(ATM_SHM mem, const char *key);

/* High-level API */

/* returns NULL if something goes wrong.
   Maybe have some variants of this: blocking, crashing, ... */
extern struct ATM_VALUE *atm_shm_get(const char *id, const char *key);
extern struct ATM_VALUE *atm_shm_get_by_url(const char *url);

/*
 * Shared memor interface - server side
 */

extern ATM_SHM atm_shm_stdmem; /* Set to first shared memory created */
extern ATM_SHM atm_shm_create(const char *id);
extern struct ATM_VALUE *atm_shm_register(ATM_SHM mem, const char *key);

/* Convenience function to update timestamp automatically */
extern void atm_shm_update(struct ATM_VALUE *var, int value);

/*
 * Filters (data processing)
 */

struct ATM_FILTER_SCALE_OFFSET {
	float scale;
	int offset;
};

struct ATM_FILTER_MOVING_AVERAGE {
	int length;
	int pos;
	int *data;
	int value;
};

/* Drop values that are far from the average */
struct ATM_FILTER_VARIANCE {
	int length;
	int pos;
	struct _ATM_VARIANCE_DATA *data;
	int value;
	unsigned int variance;
	int valid;            /* Number of valid data points */
	unsigned int squnit;  /* Typical difference between close values squared */
};

/* Use simple linear regression:
 *  * Predict the value at current time
 *  * Drop values that are too far from the prediction
 *  * Make slope available in shared memory
 */
struct ATM_FILTER_SLR {
	struct _ATM_SLR_DATA *data;
/*	struct ATM_VALUE *slope_num;
	struct ATM_VALUE *slope_denom; -- TODO */
	unsigned int length;
	int pos;
	int time_offset;
	int timesum;
	unsigned int time_variance;
	int value_offset;
	int value;
	unsigned int variance;
	int correl_sum;
	int b0;
	int b1_num;
	int b1_denom;  /* Should always be positive, but needs sign for correct arithmetic */
	int valid;
	unsigned int squnit;  /* Typical difference between close values squared */
};

/* Get the value of the currect regression line at 'time': */
extern int atm_filter_slr_estimate(struct ATM_FILTER_SLR *filter, int time);

/* The following functions return INT_MAX on "invalid" values: */
extern int atm_filter_scale_offset(void *f_scale_offset, int value);
extern int atm_filter_moving_average(void *f_mov_average, int value);
extern int atm_filter_variance(void *f_variance, int value);
extern int atm_filter_slr(void *f_slr, int value);
/* Noop, logging to a stream as side effect: */
extern int atm_filter_logger(void *stream, int value);

extern struct ATM_FILTER_MOVING_AVERAGE *atm_filter_moving_average_create
						(int length, int init_value);
extern struct ATM_FILTER_VARIANCE *atm_filter_variance_create(int length);
extern struct ATM_FILTER_SLR *atm_filter_slr_create(int length);

/*
 * Date source (shm, iio, file, ...) interface
 */

struct ATM_DSGRP_PD_RESET {
	int delay;
	int status;
	void (*disable) (void *);
	void (*enable) (void *);
	void *disable_data;
	void *enable_data;
};

/* A data source is just a function with some context. This might
   be chained together by whatever function to build a processing
   chain. */
struct ATM_DS_BARE {
	int (*cb) (void *, atm_shm_t *);
	void *private_data;
};

/* At the end of the processing chain, we need a target value to update: */
struct ATM_DS {
	struct ATM_DS_BARE src;
	struct ATM_VALUE *dest;
};

/* This describes a data source chained into a filter. Together with the
   following callback this is itself a data source again: */
struct ATM_DS_FILTER {
	struct ATM_DS_BARE src;
	void *data;
	int (*func)(void *, int);
};

extern int atm_ds_filter_cb(struct ATM_DS_FILTER *transform, int *v);

static inline void atm_ds_set_char(struct ATM_DS_BARE *ds, const char *pdata,
				   int (*cb) (const char *, int *)) {
	ds->private_data = (void *) pdata;
	ds->cb = (void *) cb;
}

static inline void atm_ds_set_dsfilter(struct ATM_DS_BARE *ds,
				       struct ATM_DS_FILTER *transform) {
	ds->private_data = (void *) transform;
	ds->cb = (void *) atm_ds_filter_cb;
}


struct ATM_DSGRP {
	struct ATM_DS *memb;
	int n;
	int cur;
	int size;

	/* This function returns:
	 *  0: nominal operation
	 * >0: request to restart after ret milli seconds
	 * <0: error condition
	 */
	int (*policy) (struct ATM_DSGRP *);
	void *policy_data;
};

extern void atm_dsgrp_init(struct ATM_DSGRP *grp);
/* Return the next free ATM_DS entry, but without actually
   incrementing the counter. User atm_dsgrp_insert() for that. */
extern struct ATM_DS *atm_dsgrp_next(struct ATM_DSGRP *grp);
extern struct ATM_DSGRP *atm_dsgrp_dsgrp(struct ATM_DSGRP *grp);
extern struct ATM_DS *atm_dsgrp_insert(struct ATM_DSGRP *grp, void *cb,
				       void *pdata, struct ATM_VALUE *dest);

extern struct ATM_VALUE *
	atm_ds_register(struct ATM_DSGRP *grp, char *url, const char *key);
extern struct ATM_VALUE *atm_ds(char *url, const char *key);
/* Setup chained filters from 'url' and return the last (ie. actual) data
   source -- typically some kind of dummy. This is useful for overwriting
   the data source in the caller, when you want configureable filters on
   top of a custom data source unsupported by libautomation. */
extern struct ATM_DS_BARE *atm_setup_filterchain(struct ATM_DSGRP *grp, char *url,
						 const char *key, struct ATM_VALUE **dest);


/*
 * Policies how a data source group deals with errors:
 */
/* Ignore any errors (but restart requests are honored of course): */
extern int atm_ds_policy_ignore(struct ATM_DSGRP *grp);

/* Assume if one value can't be read, none can. Return on the first
 * error, to prevent timeouts from accumulating: */
extern int atm_ds_policy_detect(struct ATM_DSGRP *grp);

/* Call a callback (to reset the bus of the sensor): */
extern int atm_ds_policy_reset(struct ATM_DSGRP *grp);

/* Retry a given number of times: */
extern int atm_ds_policy_retry(struct ATM_DSGRP *grp);

/* Helper for implementing custom policies: */
extern int atm_ds_policyhelper(struct ATM_DS *ds);


/*
 * Tasks
 */

#define ATM_TIMER_RES 0.001

struct ATM_TASK {
	ev_timer timer;
	struct ATM_DSGRP dsgrp;
	void (*task) (void);
};

extern void atm_task_init(struct ATM_TASK *task, ev_tstamp interval);

static inline void atm_task_start(struct ATM_TASK *task) {
	ev_timer_start(EV_A_ (ev_timer *) task);
}

/*
 * (Linux) Input devices - not quite clear how (if) this can be generalized
 * to other types of input events - like lcdproc key presses and menu events.
 */

struct input_event;

struct ATM_INPUT {
	ev_io watcher;
	void (*cb) (struct input_event *e);
};

/* return NULL if pattern can't be opened for reading, log errormessage */
extern struct ATM_INPUT *atm_input_by_path(const char *path_pattern,
					  void (*cb)(struct input_event *));

#endif
