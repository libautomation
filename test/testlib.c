#include <stdio.h>

#include "../lib/libautomation.h"

/* This is not exported globally, so we need to make it
available here: */
#include "../lib/managed_data.h"
extern struct MD_LUT _atm_shm_lut;

int test_int(int a, int b, const char *text) {
	fputs(text, stdout);
	if (a == b) {
		puts(" ok");
		return 0;
	} else {
		printf(" FAILED: %d != %d\n", a, b);
		return 1;
	}
}

int test_p(void *a, void *b, const char *text) {
	fputs(text, stdout);
	if (a == b) {
		puts(" ok");
		return 0;
	} else {
		puts(" FAILED");
		return 1;
	}
}



int test_not_NULL(void *p, const char *text) {
	fputs(text, stdout);
	if (p) {
		puts(" ok");
		return 0;
	} else {
		puts(" FAILED");
		return 1;
	}
}

int main() {
	int error_count = 0;
	ATM_SHM shmem, test_mem;
	struct ATM_VALUE *test_value, *test_value2;

	puts("Testing internal shared memory cache:");
	error_count += test_int(_atm_shm_lut.n, 0, "Initialization:");

	shmem = atm_shm_create("test");
	error_count += test_not_NULL(shmem, "Shmem creation:");

	test_value = atm_shm_get("test", "key");
	error_count += test_p(test_value, NULL, "Return NULL on missing key:");
	error_count += test_int(_atm_shm_lut.n, 1, "Add shared memory to cache:");

	test_value = atm_shm_register(shmem, "test");
	error_count += test_not_NULL(test_value, "Value createion:");

	test_value2 = atm_shm_get("test", "test");
	error_count += test_not_NULL(test_value2, "Find existing key:");
	error_count += test_int(_atm_shm_lut.n, 1, "Use cached shared memory region:");

	printf("Total Errors: %d\n", error_count);

	return error_count;
}
