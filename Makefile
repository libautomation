CC ?= cc

libautomation.so:
	make -C lib
	cp lib/libautomation.so .

link: libautomation.so
	ln -s libautomation.so /usr/local/lib/

all:
	make -C lib
	make -C examples
	make -C tools

tests:
	make -C tools $@
	make -C test
