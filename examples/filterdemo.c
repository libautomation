#include <stdio.h>
#include <stdlib.h>

#include "../lib/libautomation.h"

/* The origin of the data: */
int read_stream(FILE *f, int *v) {
	long int res;
	char line[20];
	char *s;

	if (!fgets(line, 20, f))
		exit(0);

	res = strtol(line, &s, 10);
	++atm_time;
	if (line == s)
		return -1;

	*v = res;
	return 0;
}

void main(int argc, char **argv) {
	ATM_SHM s;
	struct ATM_DS_BARE *dsb;

	s = atm_shm_create(argv[1]);
	if (!s)
		atm_fail("Couldn't open shared memory for writing");

	dsb = atm_setup_filterchain(&atm_main_task.dsgrp, argv[3], argv[2], NULL);
	if (!dsb)
		atm_fail("Couldn't register filter");

	if (dsb->cb == (int (*)(void *, volatile atm_shm_t *)) atm_file_integer) {
		dsb->private_data = fopen((char *) dsb->private_data, "r");
		dsb->cb = (int (*)(void *, volatile atm_shm_t *)) read_stream;
	}
	else {
		dsb->private_data = NULL;
	}

	if (!dsb->private_data)
		atm_fail("No input file available");

/*
 *	Plain version without filters:
 *	atm_dsgrp_insert(&atm_main_task.dsgrp, read_stream, stdin, p);
 */

/*
 * The manual way:
 *
 *	struct ATM_DSTRF_FILTER transf1, transf2, transf3;
 *
 *	transf1.src.private_data = stdin;
 *	transf1.src.cb = read_stream;
 *	transf1.data = filter1;
 *	transf1.func = filter1_func;
 *
 *	atm_ds_set_trf_filter(&transf2.src, &transf1);
 *	transf2.data = filter2;
 *	transf2.func = filter2_func;
 *
 *	atm_ds_set_trf_filter(&transf3.src, &transf2);
 *	transf3.data = filter3;
 *	transf3.func = filter3_func;
 *
 *	atm_dsgrp_insert(&atm_main_task.dsgrp, atm_ds_trf_filter, &transf3, p);
 */

/*
 * The alternative API: (Nicht gut, besser Array von ATM_DS_BARE.)
 *
 * struct FILTERS {
 *	struct ATM_DS_BARE filter;
 *	struct FILTERS *next;
 * } datasource, transf1, transf2, transf3
 *
 *	datasource.filter.private_data = stdin;
 *	datasource.filter.cb = read_stream;
 *	datasource.next = NULL;
 *	transf1.filter.private_data = filter1;
 *      transf1.filter.cb = filter1_func;
 *	transf1.next = &datasource;
 *      transf2.filter.private_data = filter2;
 *      transf2.filter.cb = filter2_func;
 *	transf2.next = &transf1;
 *      transf3.filter.private_data = filter3;
 *      transf3.filter.cb = filter3_func;
 *	transf3.next = &transf2;
 *
 *	atm_dsgrp_insert(&atm_main_task.dsgrp, atm_filter_walker, transf3, p);
 */

	while (1) {
		atm_main_task.dsgrp.policy(&atm_main_task.dsgrp);
	}
}
