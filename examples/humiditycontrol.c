#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <linux/input.h>
#include <ev.h>

#include "../lib/libautomation.h"

/* temp ... Temperature in millidegrees celsius (as available from iio)
 * return the maximum humidity in milligram per cubic meter
 */
double max_hum(int temp){
	/* constants calculated at 10 and 30 degrees celsius */
	const double T0 = 17064.67605585138, log_S0 = 8.563522217389966;

	return exp(log_S0 + temp/T0);
}

/* Global configuration variables set in read_config() */
char *fan = NULL;
int cost_factor = 7; /* heating cost. Summer: 0 */
int highhum = 80000, threshold = 500;
int timeout = 1800;
ev_tstamp interval;

/* State variables */
int fan_running = 0;
ev_tstamp manual_timeout = 0; /* Number of seconds left under manual control */

/* Other global values */
static struct ATM_VALUE *ahumi, *ahumo, *rhumi, *tempi, *rhumo, *tempo;

int control_cost(int it, int ih, int iah, int ot, int oh, int oah){
	if(iah <= oah)		/* Never rise humidity */
		return 0;
	if(ih > highhum && iah - oah > threshold) /* Avoid high humidity */
		return 1;
	if(ot > it)		/* Get free heating if possible */
		return 1;
	return ((iah - oah) * cost_factor > (it - ot)) ? 1 : 0;
}

void run_fan(int is_on) {
	if (fan && fan_running != is_on)
		atm_echo(is_on ? "1" : "0", fan);
	fan_running = is_on;
}

void main_task_fn(void){
	int ei = 0, eo = 0, validtime = atm_timestamp() - 20*interval, ret;

	eo += tempo->ts < validtime;
	eo += rhumo->ts < validtime;
	ahumo->v = (max_hum(tempo->v) * rhumo->v) / 100000;
	ei += tempi->ts < validtime;
	ei += rhumi->ts < validtime;
	ahumo->v = (max_hum(tempi->v) * rhumi->v) / 100000;
	ahumi->ts = ahumo->ts = atm_time;

	if(manual_timeout <= 0)
		run_fan(control_cost(tempi->v, rhumi->v, ahumi->v,
				     tempo->v, rhumo->v, ahumo->v));

	/* Only switch back to automatic control if we have valid data from
	   all sensors. */
	if(manual_timeout > interval || manual_timeout > 0 && ei + eo == 0)
		manual_timeout -= interval;
}

void process_event(struct input_event *e){
	if(e->type == EV_KEY && e->value == 0){
		run_fan(!fan_running); /* toggle fan */
		manual_timeout = timeout;
	}
}

void read_config(const char *filename){
	int i;
	char *line = NULL, *first, *rest;
	size_t len = 0;
	FILE *f;

	f = fopen(filename, "r");
	if(!f)
		atm_fail(filename);

	while(getline(&line, &len, f) > 0){
		if(line[0] == '#')
			continue;

		first = strtok(line, " \t");
		rest = strtok(NULL, "\n");

		if(!strcmp(first, "out_temp"))
			tempo = atm_ds(rest, first);
		else if(!strcmp(first, "out_hum"))
			rhumo = atm_ds(rest, first);
		else if(!strcmp(first, "in_temp"))
			tempi = atm_ds(rest, first);
		else if(!strcmp(first, "in_hum"))
			rhumi = atm_ds(rest, first);
		else if(!strcmp(first, "fan"))
			fan = atm_globdup(rest);
		else if(!strcmp(first, "interval"))
			interval = atm_read_float_or_fail(rest, first);
		else if(!strcmp(first, "timeout"))
			timeout = atm_read_int_or_fail(rest, "can't parse timeout");
		else if(!strcmp(first, "costfactor")){
			i = strtol(rest, &first, 10);
			if(first == rest)
				atm_log("can't parse costfactor\n");
			else
				cost_factor = i;
		}
		else if(!strcmp(first, "highhumidity"))
			highhum = atm_read_int_or_fail(rest, first);
		else if(!strcmp(first, "threshold"))
			threshold = atm_read_int_or_fail(rest, first);
		else if(!strcmp(first, "evdev"))
			atm_input_by_path(rest, process_event);
		else
			atm_log("ignoring unknown config option '%s'", first);
	}

	free(line);
	fclose(f);
}

int main(int argc, char **argv){
	char *configfile, *p;
	ATM_SHM shmem;

	if(!argv[1])
		configfile = "/etc/humiditycontrol.conf";
	else
		configfile = argv[1];

	p = strrchr(configfile, '/'); /* Get the filename without path */
	if (!p)
		p = configfile;
	else
		++p;
	shmem = atm_shm_create(p);

	read_config(configfile);

	ahumi = atm_shm_register(shmem, "ahumi");
	ahumo = atm_shm_register(shmem, "ahumo");

	atm_main_task_timer->repeat = interval;
	atm_main_task.task = main_task_fn;

	return atm_main(argc, argv);
}
