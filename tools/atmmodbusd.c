#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <ev.h>
#include <modbus/modbus.h>

#include "../lib/libautomation.h"

#define CONV_SIGNED 0x80

struct FULL_ADDR {
	uint16_t addr;
	uint8_t slave;
	uint8_t conv; /* Encode how to convert 16bit registers to 32bit values */
} uptime4 = {0, 0};

union FULL_ADDR_CONTAINER {
	struct FULL_ADDR d;
	void *c;
};

modbus_t *ctx;
int txenable = -1, rxenable = -1;

/* Parse an address of the form <slaveID>:<register>[s][*factor]
 * return: 0 success
 *         -1 error
 */
static int parse_address(char *str, struct FULL_ADDR *t) {
	char *s2, *conversion;
	int s = 0;

	t->slave = strtol(str, &s2, 0);
	if (*s2 != ':')
		return -1;
	s2++;

	t->addr = strtol(s2, &conversion, 0);
	if (*conversion == 's') {
		s = 1;
		++conversion;
	}
	if (*conversion == 0)
		t->conv = 1;
	else if (*conversion == '*')
		t->conv = strtol(++conversion, NULL, 0);
	else
		return -1;

	if (s)
		t->conv |= CONV_SIGNED;

	return 0;
}

static int convert(uint16_t v16, uint8_t conv) {
	if (conv & CONV_SIGNED)
		return (int16_t) v16 * ((int) (conv & ~CONV_SIGNED));
	else
		return v16 * (int) conv;
}

static int modbus_cb(void *private_data, int *v) {
	union FULL_ADDR_CONTAINER target;
	uint16_t v16;

	target.c = private_data;

	modbus_set_slave(ctx, target.d.slave);
	if (modbus_read_registers(ctx, target.d.addr, 1, &v16) != 1) {
		atm_log("Failing to read %d:%d - %s", target.d.slave,
				target.d.addr, modbus_strerror(errno));
		return -1;
	}
	*v = convert(v16, target.d.conv);

	return 0;
}

static int modbus4_cb(void *private_data, int *v) {
	union FULL_ADDR_CONTAINER target;
	uint16_t v16;

	target.c = private_data;

	modbus_set_slave(ctx, target.d.slave);
	modbus_flush(ctx);
	if (modbus_read_input_registers(ctx, target.d.addr, 1, &v16) != 1) {
		atm_log("Failing to read input register %d:%d - %s", target.d.slave,
				target.d.addr, modbus_strerror(errno));
		return -1;
	}
	*v = convert(v16, target.d.conv);

	return 0;
}

static int modbus32_cb(void *private_data, int *v) {
	union FULL_ADDR_CONTAINER target;
	uint16_t regs[2];

	target.c = private_data;

	modbus_set_slave(ctx, target.d.slave);
	if (modbus_read_registers(ctx, target.d.addr, 2, regs) != 2) {
		atm_log("Failing to read %d:%d - %s", target.d.slave,
				target.d.addr, modbus_strerror(errno));
		return -1;
	}
	*v = MODBUS_GET_INT32_FROM_INT16(regs, 0) * (int) (target.d.conv & ~CONV_SIGNED);

	return 0;
}

struct OUTPUT_WATCHER {
	ev_io watcher;
	struct FULL_ADDR target;
	struct OUTPUT_WATCHER *retry; /* NULL when inactive, else next output in retry chain */
	uint16_t value;
} *retries = NULL;  /* Last output that has been retried */

static void retry_add(struct OUTPUT_WATCHER *p) {
	if (p->retry)
		return;

	if (!retries) {
		p->retry = p;  /* Mark output as active */
	}
	else {
		p->retry = retries->retry;  /* Mark output as active */
		retries->retry = p;
	}

	retries = p;
}

static void retry_clear(struct OUTPUT_WATCHER *p) {
	if (!p->retry)
		return;

	if (p->retry == p)
		retries = NULL;
	else {
		/* In the common case, this is already satisfied: */
		while (retries->retry != p)
			retries = retries->retry;
		retries->retry = p->retry;
	}

	p->retry = NULL;  /* Mark output as inactive */
}

static void retry_cb() {
	static uint16_t uptime = 0;

	if (uptime4.slave && uptime != atm_time / 600) {
		uptime = atm_time / 600;
		modbus_set_slave(ctx, uptime4.slave);
		modbus_write_register(ctx, uptime4.addr, uptime);
	}

	if (!retries)
		return;

	struct OUTPUT_WATCHER *p = retries->retry;

	modbus_set_slave(ctx, p->target.slave);
	if (modbus_write_register(ctx, p->target.addr, p->value) == 1) {
		retry_clear(p);
		atm_log("output to %d:%d completed after retrying", p->target.slave, p->target.addr);
	}
	else {
		retries = p;
	}
}

static void output_cb(EV_P_ ev_io *w, int revents) {
	struct OUTPUT_WATCHER *p = (struct OUTPUT_WATCHER *) w;
	char buffer[20]; /* Should be enough for 16 bit even in binary */
	int ret;

	ret = read(w->fd, buffer, sizeof(buffer));
	if (ret < 1)
		return;

	errno = 0;
	ret = strtol(buffer, NULL, 0) / (p->target.conv & ~CONV_SIGNED);
	if (errno != 0)
		return;

	modbus_set_slave(ctx, p->target.slave);
	if (modbus_write_register(ctx, p->target.addr, ret) != 1) {
		atm_log("output to %d:%d - %s", p->target.slave, p->target.addr, modbus_strerror(errno));
		p->value = ret;
		retry_add(p);
	}
	else {
		retry_clear(p);
	}
}

static void setup_output(char *fifo_path, struct FULL_ADDR target) {
	struct OUTPUT_WATCHER *o;
	int fd;

	mkfifo(fifo_path, S_IRWXU);
	if ((fd = open(fifo_path, O_RDONLY | O_NONBLOCK)) < 0) {
		atm_log("Can't open %s", fifo_path);
		return;
	}

	/* Avoid getting EOF when using the fifo from the shell: */
	open(fifo_path, O_NONBLOCK | O_WRONLY);

	o = malloc(sizeof(*o));
	o->target = target;
	o->retry = NULL;
	ev_io_init(&o->watcher, output_cb, fd, EV_READ);
	ev_io_start(EV_A_ &o->watcher);
}

void set_rts(modbus_t *c, int on) {
	if (txenable >= 0)
		write(txenable, on ? "1\n" : "0\n", 2);
	if (rxenable >= 0)
		write(rxenable, on ? "0\n" : "1\n", 2);
}

static void parse_file(const char *name) {
	struct ATM_DSGRP *grp = &atm_main_task.dsgrp;
	FILE *f;
	char *line = NULL, stdbase[512], *stdbase_end, *first, *rest;
	union FULL_ADDR_CONTAINER target;
	size_t len;

	stdbase_end = stpcpy(stdbase, "/tmp/");
	f = fopen(name, "r");
	if (!f)
		atm_fail(name);

	while (getline(&line, &len, f) > 1) {
		if(line[0] == '#')
			continue;

		first = strtok(line, " \t\n");
		rest = strtok(NULL, "\n");

		if (!strcmp(first, "uptime4")) {
			parse_address(rest, &uptime4);
		}
		else if (!strcmp(first, "input")) {
			char *key = strtok(rest, " \t");
			if (parse_address(strtok(NULL, "\n"), &target.d))
				atm_fail(key);

			atm_dsgrp_insert(grp, modbus_cb, target.c, atm_value(key));
		}
		else if (!strcmp(first, "input4")) {
			char *key = strtok(rest, " \t");
			if (parse_address(strtok(NULL, "\n"), &target.d))
				atm_fail(key);

			atm_dsgrp_insert(grp, modbus4_cb, target.c, atm_value(key));
		}
		else if (!strcmp(first, "input32")) {
			char *key = strtok(rest, " \t");
			if (parse_address(strtok(NULL, "\n"), &target.d))
				atm_fail(key);

			atm_dsgrp_insert(grp, modbus32_cb, target.c, atm_value(key));
		}
		else if (!strcmp(first, "output")) {
			if (parse_address(strtok(rest, " \t"), &target.d))
				atm_fail(rest);

			strncpy(stdbase_end, strtok(NULL, "\n"), 256);
			setup_output(stdbase, target.d);
		}
		else if (!strcmp(first, "both")) {
			char *key = strtok(rest, " \t");
			if (parse_address(strtok(NULL, "\n"), &target.d))
				atm_fail(key);

			atm_dsgrp_insert(grp, modbus_cb, target.c, atm_value(key));

			strncpy(stdbase_end, key, 256);
			setup_output(stdbase, target.d);
		}
		else if (!strcmp(first, "group")) {
			grp = atm_dsgrp_dsgrp(&atm_main_task.dsgrp);
			grp->policy = atm_ds_policy_detect;
		}
		else if (!strcmp(first, "rtu")) {
			ctx = modbus_new_rtu(rest, 9600, 'N', 8, 2);
			if (modbus_connect(ctx) == -1)
				atm_fail(rest);
			modbus_rtu_set_custom_rts(ctx, set_rts);
			modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485);
			modbus_rtu_set_rts(ctx, MODBUS_RTU_RTS_UP);
		}
		else if (!strcmp(first, "interval")) {
			(*(ev_timer *) &atm_main_task).repeat =
				atm_read_int_or_fail(rest, first);
		}
		else if(!strcmp(first, "txenable")) {
			txenable = open(rest, O_WRONLY);
			if (txenable < 0)
				atm_log("txenable: Can't open '%s'", rest);
		}
		else if(!strcmp(first, "rxenable")) {
			rxenable = open(rest, O_WRONLY);
			if (rxenable < 0)
				atm_log("rxenable: Can't match '%s'", rest);
		}
		else if(!strcmp(first, "stdbase")) {
			stdbase_end = stpcpy(stdbase, rest);
		}
		else if(!strcmp(first, "debug")) {
			modbus_set_debug(ctx, 1);
		}
		else
			atm_log("Unknown command: %s", first);
	}

	free(line);
	fclose(f);
}

#ifdef BUILD_TEST

#include <assert.h>

int main(void) {
	struct OUTPUT_WATCHER w[2];

	/* Bug present in 6c3b0b50c3232e62c0dd5d781d1635e985ba21f1 */

	w[0].retry = NULL;
	retry_add(&w[0]);
	w[1].retry = NULL;
	retry_add(&w[1]);
	retry_clear(&w[1]);
	assert(retries->retry == retries);

	return 0;
}

#else

int main(int argc, char **argv) {
	int ret;
	const char *p;

	if(!argv[1])
		atm_fail("atmmodbusd called without argument - exiting");

	p = strrchr(argv[1], '/'); /* Get the filename without path */
	if (!p)
		p = argv[1];
	else
		++p;
	atm_shm_create(p);

	parse_file(argv[1]);

	atm_log("start");
	ret = atm_main(argc, argv);
	atm_log("all tasks completed!");

	return ret;
}

#endif
