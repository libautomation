#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <search.h>
#include <arpa/inet.h>

#include "../lib/libautomation.h"

struct ATM_VALUE **values;
int *last_times = NULL;
int num_values = 0, init_time, start_time;
int logid = -1;
char *logname = NULL;
FILE *ascii = NULL, *binary = NULL, *input = NULL, *ts = NULL, *csv = NULL;

struct DATA {
	short index;
	unsigned short delta;
	atm_shm_t value;
};

struct KEY {
	struct ATM_VALUE *mem;
	int value_index;
};

static int read_update_binary(struct DATA *data) {
	int ret;

	ret = fread(data, sizeof(*data), 1, input);
	data->index = ntohs(data->index);
	data->delta = ntohs(data->delta);
	data->value = ntohl(data->value);

	return ret;
}

static int read_update_text(struct DATA *data) {
	return fscanf(input, "%hd %hd %d\n", &data->index, &data->delta, &data->value);
}

int(*read_update)(struct DATA *);

static int mygetline(char **line_ptr, size_t *line_n, FILE *stream) {
	int len, c;

	if (*line_n == 0) {
		*line_n = 128;
		*line_ptr = malloc(*line_n);
	}

	for (len = 0; (c = fgetc(stream)) > 0 && c != '\n'; ++len) {
		(*line_ptr)[len] = c;
		if (len >= *line_n) {
			*line_n <<= 1;
			*line_ptr = realloc(*line_ptr, *line_n);
		}
	}

	(*line_ptr)[len] = 0;
	if (c == '\n')
		++len;

	return len;
}

static int lfind_strcmp(const void *key, const void *objp) {
	return strcmp((const char *) key, *(const char **) objp);
}

int main(int argc, char **argv) {
	struct DATA data, timer_overrun_data = {-1, USHRT_MAX, 0};
	ATM_SHM mem = NULL;
	int i, all = 0, num_keys = 0, max_keys = 16, len, delta, interval;
	int now, base_time, csv_time;
	size_t line_n = 0, num_args;
	struct KEY *keys;
	struct ATM_VALUE temp_value;
	char *line = NULL;

	i = 1;

	while (argv[i] && argv[i][0] == '-') {
		switch (argv[i][1]) {
		case 'a':
			all = 1;
			break;

		case 'd':
			interval = strtol(&argv[i][3], NULL, 0);
			++i;
			csv = fopen(argv[i], "w");
			if (!csv)
				atm_log("Can't open csv file '%s' for output", &argv[i]);
			break;

		case 'f':
			binary = fopen(&argv[i][3], "w");
			if (!binary)
				atm_log("Can't open output file '%s'", &argv[i][3]);
			break;

		case 'l':  /* log this key as time series to the following file */
			logname = &argv[i][3];
			++i;
			ts = fopen(argv[i], "w");
			if (!ts)
				atm_log("Can't open time series output file '%s'", &argv[i]);
			break;
		case 'm':
			mem = atm_shm_create(&argv[i][3]);
			if (!mem)
				atm_log("Can't open shared memory '%s' for writing", &argv[i][3]);
			break;
	
		case 't':
			ascii = fopen(&argv[i][3], "w");
			if (!ascii)
				atm_log("Can't open output file '%s'", &argv[i][3]);
			break;

		default:
			atm_log("Unkown option '%s'", argv[i]);
		}

		i++;
	}

	if (!argv[i])
		atm_fail("atmreplay needs an input file");
	input = fopen(argv[i], "r");
	if (!input)
		atm_fail("Can't open input file");
	++i;

	keys = malloc(sizeof(*keys) * max_keys);

	puts("Present keys:");
	if (csv)
		fprintf(csv, "timestamp");

	num_args = argc - i;
	for (num_keys = 0; (len = mygetline(&line, &line_n, input)) > 1; ++num_keys) {
		if (num_keys >= max_keys) {
			max_keys <<= 1;
			keys = realloc(keys, sizeof(*keys) * max_keys);
		}

		puts(line);
		if (logname && !strcmp(line, logname))
			logid = num_keys;
		if (!(all || lfind(line, &argv[i], &num_args, sizeof(argv[i]), lfind_strcmp))){
			keys[num_keys].value_index = -1;
			continue;
		}

		if (mem)
			keys[num_keys].mem = atm_shm_register(mem, line);
		else if (csv)
			keys[num_keys].mem = atm_value(NULL);

		if (ascii)
			fprintf(ascii, "%s\n", line);
		if (binary)
			fprintf(binary, "%s\n", line);
		if (csv)
			fprintf(csv, ",%s", line);
		keys[num_keys].value_index = num_values;
		++num_values;
	}

	if (len == 1) {
		read_update = read_update_text;
		fscanf(input, "%d\n", &i);
		printf("in text file with %d keys\n", i);
	}
	else if (len == 0) {
		read_update = read_update_binary;
		fread(&i, sizeof(i), 1, input);
		printf("in binary file with %d keys\n", i);
	}
	else {
		atm_fail("File corrupt at text/binary marker");
	}

	if (i != num_keys)
		atm_fail("File corrupt: Number of keys doesn't match expectations");

	if (binary) {
		fputc(0, binary);
		fwrite(&num_values, sizeof(num_values), 1, binary);
	}
	if (ascii) {
		fputc('\n', ascii);
		fprintf(ascii, "%d\n", num_values);
	}
	if (csv)
		fputc('\n', csv);

	/* Read initial values */
	init_time = atm_timestamp();
	for (i = 0; i < num_keys; ++i) {
		if (len == 1)	/* Text input */
			fscanf(input, "%d %d\n", &temp_value.ts, &temp_value.v);
		else
			fread(&temp_value, sizeof(temp_value), 1, input);
		if (keys[i].value_index == -1)
			continue;

		if (binary)
			fwrite(&temp_value, sizeof(temp_value), 1, binary);
		if (ascii)
			fprintf(ascii, "%d\t%d\n", temp_value.ts, temp_value.v);
		if (mem || csv) {
			keys[i].mem->ts = init_time;
			keys[i].mem->v = temp_value.v;
		}
	}
	
	/* Read recording start time */
	if (len == 1)   /* Text input */
		fscanf(input, "%d\n", &start_time);
	else
		fread(&start_time, sizeof(start_time), 1, input);

	printf("Start time stamp: %d\n", start_time);
	csv_time = start_time;
	if (binary)
		fwrite(&start_time, sizeof(start_time), 1, binary);
	if (ascii)
		fprintf(ascii, "%d\n", start_time);
	if (csv) {
		fprintf(csv, "%f", (float) csv_time / 10.0);
		for (i = 0; i < num_keys; ++i)
			if (keys[i].value_index != -1)
				fprintf(csv, ",%d", keys[i].mem->v);
		fputc('\n', csv);
		csv_time += interval;
	}

	/* Loop over all updates */
	delta = 0;
	base_time = start_time;
	now = init_time;
	while (read_update(&data) > 0) {
		delta += data.delta;
		if (csv && base_time + delta > csv_time) {
			fprintf(csv, "%f", (float) csv_time / 10.0);
			for (i = 0; i < num_keys; ++i)
				if (keys[i].value_index != -1)
					fprintf(csv, ",%d", keys[i].mem->v);
			fputc('\n', csv);
			csv_time += interval;
		}

		if (data.index == logid && ts)
			fprintf(ts, "%d\t%d\n", base_time + delta, data.value);
		if (data.index < 0 || keys[data.index].value_index == -1)
			continue;

		struct ATM_VALUE *target = keys[data.index].mem;
		base_time += delta;
		if (ascii)
			fprintf(ascii, "%d\t%d\t%d\n", keys[data.index].value_index, delta, data.value);
		if (binary) {
			for (i = USHRT_MAX; delta > i; i += USHRT_MAX)
				fwrite(&timer_overrun_data, sizeof(timer_overrun_data), 1, binary);
			i -= USHRT_MAX;
			data.delta = delta - i;
			data.index = keys[data.index].value_index;
			fwrite(&data, sizeof(data), 1, binary);
		}
		if (mem || csv) {
			if (delta && mem) {
				fflush(NULL);
				ev_sleep(0.1 * (base_time - start_time + init_time - now));
				now = atm_timestamp();
			}
			target->ts = now;
			target->v = data.value;
		}
		delta = 0;
	}

	printf("Final timestamp: %d\n", base_time);
		
	return 0;
}
