#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <arpa/inet.h>

#include "../lib/libautomation.h"

static struct ATM_VALUE **values;
static char *latest_cache;
static int last_time, latest;
static int num_values = 0;
static FILE *output;

struct DATA {
	uint16_t index;
	uint16_t delta;
	int32_t value;
};

static int delta_comp(const void *pa, const void *pb) {
	int a = *(const int *)pa;
	int b = *(const int *)pb;

	return values[a]->ts - values[b]->ts;
}

static void main_task(void) {
	struct DATA data;
	int indices[num_values], i, updates = 0;
	uint16_t index, delta;
	int32_t value;

	for (i = 0; i < num_values; ++i)
		if (values[i]->ts > last_time || (values[i]->ts == last_time && !latest_cache[i]))
			indices[updates++] = i;

	if (!updates)
		return;

	qsort(indices, updates, sizeof(indices[0]), delta_comp);
	latest = values[indices[updates-1]]->ts;
	if (latest > last_time)
		memset(latest_cache, 0, num_values);
	i = 0;
	while (i < updates) {
		index = indices[i];
		if (values[index]->ts - last_time >= USHRT_MAX) {
			index = USHRT_MAX;
			delta = USHRT_MAX;
			value = 0;
			last_time += USHRT_MAX;
		}
		else {
			delta = values[index]->ts - last_time;
			value = values[index]->v;
			last_time = values[index]->ts;
			if (last_time == latest)
				latest_cache[index] = 1;
			++i;
		}
		data.index = htons(index);
		data.delta = htons(delta);
		data.value = htonl(value);
		fwrite(&data, sizeof(data), 1, output);
	}
			
	fflush(output);
}

int main(int argc, char **argv) {
	ATM_SHM mem;
	int i, freq = 10;

	i = 1;

	values = malloc(sizeof(*values) * argc);

	if (argv[i] && sscanf(argv[i], "-f=%d", &freq) == 1)
		i++;

	if (!argv[i])
		atm_fail("atmrecord needs an output file");
	output = fopen(argv[i], "w");
	if (!output)
		atm_fail("Can't open output file");

	++i;
	while (argv[i]) {
		mem = atm_shm_attach(argv[i]);
		if (!mem)
			fprintf(stderr, "atmrecord can't attach shared memory for '%s'", argv[i]);

		while (argv[++i]) {
			if (argv[i][0] == '-') {
				++i;
				break;
			}

			values[num_values] = atm_shm_find(mem, argv[i]);
			if (values[num_values]) {
				++num_values;
				fprintf(output, "%s\n", argv[i]);
			}
			else {
				fprintf(stderr, "Can't find key '%s' - ignoring", argv[i]);
			}
		}
	}

	fputc(0, output);

	fwrite(&num_values, sizeof(num_values), 1, output);
	for (i = 0; i < num_values; ++i)
		fwrite(values[i], sizeof(*values[i]), 1, output);

	last_time = atm_timestamp();
	fwrite(&last_time, sizeof(last_time), 1, output);

	latest = last_time;
	latest_cache = malloc(num_values);
	memset(latest_cache, 1, num_values);

	atm_main_task_timer->repeat = 0.1 * freq;
	atm_main_task.task = main_task;

	return atm_main(argc, argv);
}
