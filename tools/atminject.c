#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ev.h>

#include "../lib/libautomation.h"


int main(int argc, char **argv) {
	int v;
	ATM_SHM s;
	struct ATM_VALUE *p;

	if(argc != 4)
		atm_fail("atminject <domain> <key> <value>");

	errno = 0;
	v = strtol(argv[3], NULL, 0);
	if (errno) {
		perror(argv[3]);
		return -1;
	}

	s = atm_shm_create(argv[1]);
	if (!s)
		atm_fail("Couldn't open shared memory for writing");

	p = atm_shm_register(s, argv[2]);
	atm_shm_update(p, v);

	return 0;
}
