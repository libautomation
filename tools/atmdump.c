#include <string.h>
#include <stdio.h>

#include "../lib/libautomation.h"

static void default_format(const char *name, int i, int v, int ts) {
	printf("%d: '%s' - last update: %d, value %d\n", i, name, ts, v);
}

static void atmd_format(const char *name, int i, int v, int ts) {
	printf("%s value:%d\n", name, v);
}

static void csv(const char *name, int i, int v, int ts) {
	printf("%s,%d,%d\n", name, v, ts);
}

int main(int argc, char **argv) {
	void (*formatter)(const char *, int, int, int);
	ATM_SHM mem;
	const char *p;
	int i, format = 0;

	i = 1;

	if (argv[i] && argv[i][0] == '-') {
		format = argv[i][1];
		i++;
	}

	if (format == 'h' || format == '?') {
		fprintf(stderr, "Usage: atmdump [-FORMAT] SHMID\n");
		return 0;
	}

	if (!argv[i])
		atm_fail("atmdump called without argument - exiting");

	mem = atm_shm_attach(argv[i]);
	if (!mem)
		atm_fail("atmdump can't attach shared memory");

	switch (format) {
	case 'c':
		printf("name,value,timestamp\n");
		formatter = csv;
		break;
	case 'f':
		formatter = atmd_format;
		break;
	default:
		formatter = default_format;
	}

	for (i = 0, p = mem->keys; *p; ++i, p += strlen(p) + 1)
		formatter(p, i, mem->data[i].v, mem->data[i].ts);

	return 0;
}
